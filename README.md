# Marketstack test project

## Description

The NestJS project to pull data from marketstack api. It implements `/eod` route with caching responses.

## Environment variables

At the first you need to copy `.env.sample` file to `.env` and put there an `ACCESS_KEY` from marketstack api. Env sample allows to configure:
- TARGET_URL - url to make requests
- ACCESS_KEY - acces_key query parameter for requests
- CACHE_TTL(5 by default) - cache time to live in seconds
- CACHE_MAX(10 by default) - maximum number of items in cache


## Docs
Swagger doc is available on `/api` route after starting the app.

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Running in a docker container

Build docker image with tag name: 
```
docker build -t <tagName> .
```

Run docker image on 3000 port with `.env` file:
```
docker run -p 3000:80 --env-file ./.env <tagName>
```