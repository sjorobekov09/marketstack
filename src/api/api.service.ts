import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import axios, { AxiosInstance } from "axios";

export interface IApiService {
  get<TResponse>(path: string, params: Record<string, unknown>): Promise<TResponse>;
}

@Injectable()
export class ApiService {
  private client: AxiosInstance;
  private accessKey: string;

  constructor(private configService: ConfigService) {
    this.accessKey = this.configService.get<string>('accessKey');
    this.client = axios.create({
      baseURL: this.configService.get<string>('targetUrl'),
    });
  }

  async get<TResponse>(path: string, params: Record<string, unknown>): Promise<TResponse> {
    try {
      const response = await this.client.get<TResponse>(path, {
        params: {
          access_key: this.accessKey,
          ...params
        }
      });
      return response.data;
    } catch (error) {
      throw new Error(error);
    }
  }
}