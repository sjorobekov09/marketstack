import { CacheInterceptor, CacheModule, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import apiConfiguration from './config/configuration';
import { MarketstackModule } from './marketstack/marketstack.module';

@Module({
  imports: [
    ConfigModule.forRoot({ load: [apiConfiguration] }),
    CacheModule.register({
      ttl: parseInt(process.env.CACHE_TTL) || 5,
      max: parseInt(process.env.CACHE_MAX) || 10,
    }),
    MarketstackModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_INTERCEPTOR,
      useClass: CacheInterceptor,
    }
  ],
})
export class AppModule { }
