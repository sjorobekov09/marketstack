export default () => ({
  targetUrl: process.env.TARGET_URL || 'http://localhost:3000',
  accessKey: process.env.ACCESS_KEY || ''
});