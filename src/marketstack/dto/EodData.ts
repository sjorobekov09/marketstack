import { ApiProperty } from "@nestjs/swagger";

export class EodData {
  @ApiProperty()
  open: number;
  @ApiProperty()
  hight: number;
  @ApiProperty()
  close: number;
  @ApiProperty()
  low: number;
  @ApiProperty()
  split_factor: number;
  @ApiProperty()
  adj_high: number;
  @ApiProperty()
  adj_low: number;
  @ApiProperty()
  adj_close: number;
  @ApiProperty()
  adj_open: number;
  @ApiProperty()
  adj_volume: number;
  @ApiProperty()
  exchange: string;
  @ApiProperty()
  date: string;
  @ApiProperty()
  symbol: string;
}