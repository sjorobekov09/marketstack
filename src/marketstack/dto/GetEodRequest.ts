import { ApiProperty, ApiQuery } from "@nestjs/swagger";

export class GetEodRequest {
  access_key?: string;
  symbols?: string;
  @ApiProperty({ required: false })
  exchange?: string;
  @ApiProperty({ required: false })
  sort?: string;
  @ApiProperty({ required: false })
  date_from?: string;
  @ApiProperty({ required: false })
  date_to?: string;
  @ApiProperty({ required: false })
  limit?: number;
  @ApiProperty({ required: false })
  offset?: number;
}