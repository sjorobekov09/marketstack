import { ApiProperty } from "@nestjs/swagger";
import { EodData } from "./EodData";
import { GetEodResponsePagination } from "./GetEodResponsePagination";

export class GetEodResponse {
  @ApiProperty()
  pagination: GetEodResponsePagination;
  @ApiProperty({ isArray: true, type: EodData })
  data: [EodData];
}