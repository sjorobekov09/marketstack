import { ApiProperty } from "@nestjs/swagger";

export class GetEodResponsePagination {
  @ApiProperty()
  limit: number;
  @ApiProperty()
  offset: number;
  @ApiProperty()
  count: number;
  @ApiProperty()
  total: number;
}