import { Test, TestingModule } from '@nestjs/testing';
import { MarketstackController } from './marketstack.controller';

describe('MarketstackController', () => {
  let controller: MarketstackController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MarketstackController],
    }).compile();

    controller = module.get<MarketstackController>(MarketstackController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
