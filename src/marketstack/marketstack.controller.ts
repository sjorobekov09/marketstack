import { Controller, Get, Inject, Param, Query } from '@nestjs/common';
import { ApiParam, ApiQuery, ApiResponse, ApiTags } from '@nestjs/swagger';
import { ApiService } from 'src/api/api.service';
import { GetEodRequest, GetEodResponse } from './dto';

@ApiTags('Marketstack')
@Controller('/api/v1')
export class MarketstackController {
  constructor(private readonly api: ApiService) {}

  @Get('/eod/:symbols')
  @ApiParam({ name: 'symbols', example: 'AAPL' })
  @ApiResponse({ type: GetEodResponse })
  getEOD(@Param('symbols') symbols: string, @Query() query: GetEodRequest): Promise<GetEodResponse> {
    return this.api.get('/eod', { symbols, ...query });
  }
}
