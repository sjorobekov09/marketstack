import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ApiModule } from 'src/api/api.module';
import { MarketstackController } from './marketstack.controller';

@Module({
  imports: [ConfigModule, ApiModule],
  controllers: [MarketstackController],
  providers: [],
})
export class MarketstackModule { }
